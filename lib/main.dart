import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Application name
      title: 'Flutter Hello World',
      // Application theme data, you can set the colors for the application as
      // you want
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      // A widget which will be started on application startup
      home: MyHomePage(title: 'Pratikum II'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  const MyHomePage({@required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(icon: new Icon(Icons.call, color: Colors.white)),
              IconButton(icon: new Icon(Icons.search, color: Colors.white)),
            ],
            brightness: Brightness.light,
            backgroundColor: Colors.yellowAccent,
            title: Text('BelajarFlutter.com', style: TextStyle(color: Colors.purple))),

        // The title text which will be shown on the action ba
        body: Container(
            decoration: BoxDecoration(
              color: const Color(0xFF3366FF),
              image: const DecorationImage(
                image: NetworkImage('https://i.pinimg.com/originals/91/86/6b/91866b918c9cca0747f483a46943e926.jpg'),
                fit: BoxFit.cover,
              ),
              border: Border.all(
                color: Colors.red,
                width: 9,
              ),
              borderRadius: BorderRadius.circular(12),
            ),
            height: 570,
            width: 300,
            margin: EdgeInsets.all(50)));
  }
}
